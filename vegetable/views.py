from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from vegetable.models import Vegetable

class VegetableListView(LoginRequiredMixin,ListView):
    model = Vegetable
    template_name = "vegetables/list1.html"


class VegetableDetailView(LoginRequiredMixin,DetailView):
    model = Vegetable
    template_name = "vegetables/detail.html"


class VegetableCreateView(LoginRequiredMixin, CreateView):
    model = Vegetable
    template_name = "vegetables/create.html"
    fields = ["name", "unit", "price", "description","image"]

    def get_success_url(self):
        return reverse_lazy("home")

