from django.contrib import admin
from vegetable.models import Vegetable, Order

# Register your models here.
class VegetableAdmin(admin.ModelAdmin):
    readonly_fields = ('id',)
admin.site.register(Vegetable,VegetableAdmin)
admin.site.register(Order)
