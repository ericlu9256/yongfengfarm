from email.policy import default
from django.db import models
from django.core.validators import MinValueValidator
from django.contrib.auth.models import User
from django.contrib.auth.mixins import LoginRequiredMixin
# Create your models here.
class Vegetable(LoginRequiredMixin,models.Model):
    UNIT_CHOICES = (
        ('lbs','lbs'),
        ('case','case'),
        ('each','each')
    )

    name = models.CharField(max_length=150)
    unit = models.CharField(max_length=4, choices=UNIT_CHOICES)
    price = models.DecimalField(max_digits=5, decimal_places=2, validators=[MinValueValidator(0)])
    description = models.CharField(max_length=250, default="", blank=True, null=True)
    image = models.ImageField(upload_to='vegetable/media/images',blank= True, null = True)
    URLimage = models.URLField(blank=True , null= True)

    def __str__(self):
        return self.name


class Order(LoginRequiredMixin,models.Model):
    vegetable = models.ForeignKey(Vegetable, related_name="orders" ,on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(default=1)

    def __str__(self):
        return self.vegetable.name
