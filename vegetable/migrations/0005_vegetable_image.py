# Generated by Django 4.1 on 2022-08-10 04:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("vegetable", "0004_remove_vegetable_image"),
    ]

    operations = [
        migrations.AddField(
            model_name="vegetable",
            name="image",
            field=models.ImageField(
                blank=True, height_field="235", upload_to="", width_field="150"
            ),
        ),
    ]
