from django.urls import path
from django.contrib import admin

from vegetable.views import (VegetableListView,
                            VegetableDetailView,
                            VegetableCreateView,
                            )


urlpatterns = [
    path("", VegetableListView.as_view(), name="home"),
    path("<int:pk>/", VegetableDetailView.as_view(), name="show_vegetable"),
    path("create/", VegetableCreateView.as_view(), name="create_product"),
]




